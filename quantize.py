import tensorflow as tf
from PIL import Image
import numpy as np
import os

images = [np.asarray(Image.open('quant/' + file)) for file in os.listdir('quant')]

def representative_data_gen():
    for image in images:
        yield [image.astype(np.float32)]

model = tf.keras.models.load_model('save/fine_tuning.h5')

converter = tf.lite.TFLiteConverter.from_keras_model(model)
converter.optimization = [tf.lite.Optimize.DEFAULT]
converter.representative_dataset = representative_data_gen()
tflite_model = converter.convert()
with open('quantized_model.tflite', 'wb') as f:
    f.write(tflite_model)
