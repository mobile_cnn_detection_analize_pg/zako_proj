import cv2
import sys
import os

vidcap = cv2.VideoCapture(sys.argv[1])
destination_dir = sys.argv[2]
os.mkdir(destination_dir)
status, image = vidcap.read()
count = 0
while status:
    path = "{}/{}_{:04d}.jpg".format(destination_dir, os.path.basename(destination_dir), count)
    cv2.imwrite(path, image)
    status, image = vidcap.read()
    count += 1
