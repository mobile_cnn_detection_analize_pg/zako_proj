import cv2
import tensorflow as tf
from keras_applications import imagenet_utils
import numpy as np
from matplotlib import pyplot as plt
import png


model_path = 'model.tflite'

with open('labels.txt') as labels_file:
    labels = labels_file.readlines()
    labels = [label.rstrip() for label in labels]
    labels = { i: labels[i] for i, label in enumerate(labels)}

interpreter = tf.lite.Interpreter(model_path=model_path)
interpreter.allocate_tensors()

input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

input_shape = input_details[0]['shape']


cv2.namedWindow('preview')
vc = cv2.VideoCapture(0)

if vc.isOpened():
    rval, frame = vc.read()
else:
    rval = False

dim = (input_shape[1], input_shape[2])
resized_frame = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
# processed_frame = imagenet_utils.preprocess_input(resized_frame.astype(np.float32), data_format='channels_last', mode='tf')
input_data = [np.array(resized_frame, dtype=np.float32)]

books = []
while rval:
    cv2.imshow('preview', frame)
    rval, frame = vc.read()

    dim = (input_shape[1], input_shape[2])
    resized_frame = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
    processed_frame = imagenet_utils.preprocess_input(resized_frame.astype(np.float32), data_format='channels_last', mode='tf')
    input_data = [np.array(processed_frame, dtype=np.float32)]
    interpreter.set_tensor(input_details[0]['index'], input_data)
    interpreter.invoke()
    output_data = interpreter.get_tensor(output_details[0]['index'])
    output_data = np.squeeze(output_data, axis=0)
    print("predicted class: {}, i = {}".format(labels[np.argmax(output_data, axis=-1)], output_data))
    key = cv2.waitKey(20)
    if key == 27:
        break
cv2.destroyWindow('preview')
